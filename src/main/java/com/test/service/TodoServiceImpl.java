package com.test.service;

import com.test.dto.request.TodoDetailRequest;
import com.test.dto.request.TodoRequest;
import com.test.dto.response.TodoResponse;
import com.test.model.Todo;
import com.test.model.TodoDetail;
import com.test.model.TodoPanache;
import com.test.repository.TodoDetailRepository;
import com.test.repository.TodoRepository;
import com.test.repository.TodoRepositoryPanache;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class TodoServiceImpl implements TodoService {

    @Inject
    TodoRepositoryPanache repository;

    private final TodoRepository todoRepository;

    private final TodoDetailRepository todoDetailRepository;


    public TodoServiceImpl(TodoRepository todoRepository,
                           TodoDetailRepository todoDetailRepository) {

        this.todoRepository = todoRepository;
        this.todoDetailRepository = todoDetailRepository;
    }

    @Override
    @Transactional
    public TodoResponse create(TodoRequest request) {
        TodoPanache todoPanache = new TodoPanache();
        todoPanache.setName(request.getName());
        todoPanache.setDescription(request.getDescription());
        repository.persist(todoPanache);


        return convertResponse(todoPanache);
    }

    @Override
    public TodoResponse get(Long id) {
        TodoPanache todoPanache = repository.findById(id);

        return convertResponse(todoPanache);
    }

    @Override
    public TodoResponse update(TodoRequest request, Long id) {
        TodoPanache todoPanache = repository.findById(id);
        todoPanache.setName(request.getName());
        todoPanache.setDescription(request.getDescription());
        repository.persist(todoPanache);

        return convertResponse(todoPanache);
    }

    @Override
    @Transactional
    public TodoResponse createTodo(TodoRequest request) {
        Todo todo = new Todo();
        todo.setName(request.getName());
        todo.setDescription(request.getDescription());

        todoRepository.save(todo);

        for(TodoDetailRequest todoDetailRequest : request.getDetails()) {
            TodoDetail todoDetail = new TodoDetail();
            todoDetail.setCreatedAt(LocalDateTime.now());
            todoDetail.setDueDate(LocalDateTime.now());
            todoDetail.setTaskName(todoDetailRequest.getTaskName());
            todoDetail.setTodo(todo);
            todoDetailRepository.save(todoDetail);

        }

        todoRepository.save(todo);

        return convertResponseTodo(todo);
    }

    @Override
    public TodoResponse getTodo(Long id) {
        Todo todo = todoRepository.findById(id).orElseThrow();

        return convertResponseTodo(todo);
    }

    @Override
    public TodoResponse updateTodo(TodoRequest request, Long id) {
        Todo todo = todoRepository.findById(id).orElseThrow();
        todo.setName(request.getName());
        todo.setDescription(request.getDescription());
        todo.setCompleted(request.isCompleted());
        todoRepository.save(todo);

        return convertResponseTodo(todo);
    }

    @Override
    public List<TodoResponse> listTodo() {
        List<Todo> todos = todoRepository.findAll();

        return todos.stream().map(this::convertResponseTodo).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void deleteTodo(Long id) {
        Todo todo = todoRepository.findById(id).orElseThrow();
        todoRepository.delete(todo);
    }

    private TodoResponse convertResponse(TodoPanache todoPanache) {
        TodoResponse response = new TodoResponse();
        response.setId(todoPanache.id);
        response.setName(todoPanache.getName());
        response.setDescription(todoPanache.getDescription());

        return response;
    }

    private TodoResponse convertResponseTodo(Todo todoPanache) {
        TodoResponse response = new TodoResponse();
        response.setId(todoPanache.getId());
        response.setName(todoPanache.getName());
        response.setDescription(todoPanache.getDescription());

        return response;
    }
}
