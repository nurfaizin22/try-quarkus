package com.test.service;

import com.test.dto.request.TodoRequest;
import com.test.dto.response.TodoResponse;
import com.test.model.Todo;

import java.util.List;

public interface TodoService {

    TodoResponse create(TodoRequest request);

    TodoResponse get(Long id);

    TodoResponse update(TodoRequest request, Long id);

    TodoResponse createTodo(TodoRequest request);

    TodoResponse getTodo(Long id);

    TodoResponse updateTodo(TodoRequest request, Long id);

    List<TodoResponse> listTodo();

    void deleteTodo(Long id);
}
