package com.test.controller;

import com.test.dto.request.TodoRequest;
import com.test.dto.response.TodoResponse;
import com.test.service.TodoService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/todo")
public class TodoResource {

    @Inject
    TodoService todoService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello RESTEasy";
    }


    //panache
    @GET
    @Path("/test")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response todos() {
        TodoResponse response = new TodoResponse();

        response.setName("test todo");
        response.setDescription("test description 1 2");

        return Response.ok(response).build();
    }

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(TodoRequest request) {
        TodoResponse response = todoService.create(request);

        return  Response.ok(response).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Long id){
        TodoResponse response = todoService.getTodo(id);

        return Response.ok(response).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Long id, TodoRequest request) {
        TodoResponse response = todoService.update(request, id);

        return Response.ok(response).build();
    }


    //spring data jpa
    @POST
    @Path("/create-todo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveTodo(TodoRequest request) {
        TodoResponse response = todoService.createTodo(request);

        return  Response.ok(response).build();
    }

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public  Response list () {
        List<TodoResponse> responses= todoService.listTodo();

        return Response.ok(responses).build();
    }

    @PUT
    @Path("/update-todo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateTodo(TodoRequest request, @PathParam("id") Long id) {
        TodoResponse response = todoService.updateTodo(request, id);

        return  Response.ok(response).build();
    }

    @DELETE
    @Path("/delete-todo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteTodo(@PathParam("id") Long id) {
        todoService.deleteTodo( id);

        return  Response.ok().build();
    }
}