package com.test.dto.response;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@Getter @Setter
public class TodoResponse  {

    private Long id;

    private String name;

    private String description;

}
