package com.test.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TodoDetailRequest {

    private String taskName;

    private String dueDate;
}
