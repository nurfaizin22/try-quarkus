package com.test.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TodoRequest {

    private String name;

    private String description;

    private boolean completed;

    private List<TodoDetailRequest> details;
}
