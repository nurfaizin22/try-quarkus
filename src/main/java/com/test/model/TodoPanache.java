package com.test.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "todo")
@Getter
@Setter
public class TodoPanache extends PanacheEntity {

    private String name;

    private String description;

    private boolean completed;
}
