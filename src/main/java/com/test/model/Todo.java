package com.test.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "todo")
@Getter
@Setter
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String description;

    private boolean completed;

    @OneToMany(mappedBy = "todo", cascade = CascadeType.ALL)
    private Set<TodoDetail> details = new HashSet<>();
}
