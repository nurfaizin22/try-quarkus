package com.test.repository;

import com.test.model.TodoDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoDetailRepository extends JpaRepository<TodoDetail, Long> {
}
