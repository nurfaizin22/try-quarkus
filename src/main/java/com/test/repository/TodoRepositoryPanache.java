package com.test.repository;

import com.test.model.TodoPanache;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TodoRepositoryPanache implements PanacheRepository<TodoPanache> {

    public TodoPanache findById(Long id) {
        return find("id", id).firstResult();
    }

    public TodoPanache findByName(String name) {
        return find("name", name).firstResult();
    }


}
